import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { selectLogin, checkLogin } from './stores/reducers/loginSlice';
import Default from './layouts/Default';
import Slider from './components/Slider';
import MainContent from './components/MainContent';
import Login from './pages/Login';

const App = () => {
  const dispatch = useDispatch();
  const login = useSelector(selectLogin);

  useEffect(() => {
    dispatch(checkLogin());
  }, []);

  return (
    <div className="App">
      {login ? <Default slider={<Slider />} Cnt={MainContent} /> : <Login />}
    </div>
  );
};

export default App;
