import { Layout } from 'antd';
import PropTypes from 'prop-types';

import style from '../styles/layouts/Default.Module.scss';

const { Content } = Layout;

const Default = ({ slider, Cnt }) => {
  return (
    <div className={style.bg}>
      <div className={style.container}>
        <Layout>
          <Layout>
            <div className={style.smHide}>{slider}</div>
            <Content>
              <Cnt menuBtnCls={style.smVisible} />
            </Content>
          </Layout>
        </Layout>
      </div>
    </div>
  );
};

Default.propTypes = {
  slider: PropTypes.node.isRequired,
  Cnt: PropTypes.func.isRequired,
};

export default Default;
