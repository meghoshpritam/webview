/* eslint-disable import/prefer-default-export */
import { useState } from 'react';
import { post, get } from 'axios';
import { stringify } from 'qs';
import parallel from 'async/parallel';
import { useDispatch } from 'react-redux';

import { resetLogin } from '../stores/reducers/loginSlice';

const postC = async (url, data, withToken = true) => {
  let response = null;
  let error = null;
  let headers = {
    'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
  };

  if (withToken) {
    headers = { ...headers, Token: localStorage.getItem('token') };
  }

  try {
    response = await await post(url, stringify(data), {
      headers,
    });

    response = response.data;
    return [response, error];
  } catch (err) {
    error = err.response.data;
    return [response, error];
  }
};

const getC = async (url, params = {}, withToken = true) => {
  let response = null;
  let error = null;
  let headers = {
    'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
  };

  if (withToken) {
    headers = { ...headers, Token: window.localStorage.getItem('token') };
  }

  try {
    response = await get(url, { params, headers });

    response = response.data;
    return [response, error];
  } catch (err) {
    error = err.response.data;
    return [response, error];
  }
};

export const usePost = () => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const callBack = async (url, data = {}, withToken = true) => {
    setLoading(true);
    const [res, err] = await postC(url, data, withToken);

    if (res) {
      setResponse(() => res);
      setError(() => null);
    }
    if (err) {
      if (err.status_code === 401) dispatch(resetLogin());
      setError(() => err);
      setResponse(() => null);
    }

    setLoading(false);
  };

  return [response, error, loading, callBack];
};

export const useGet = () => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const callBack = async (url, params = {}, withToken = true) => {
    setLoading(true);
    const [res, err] = await getC(url, params, withToken);

    if (res) {
      setResponse(() => res);
      setError(() => null);
    }
    if (err) {
      if (err.status_code === 401) dispatch(resetLogin());
      setError(() => err);
      setResponse(() => null);
    }

    setLoading(false);
  };

  return [response, error, loading, callBack];
};

export const useParallel = () => {
  const [loading, setLoading] = useState(false);
  const [results, setResults] = useState(null);
  const dispatch = useDispatch();

  const cb = async (config = []) => {
    setLoading(() => true);
    const resp = await parallel([
      // eslint-disable-next-line consistent-return
      ...config.map((con) => async () => {
        if (con.method === 'get') {
          const [res, err] = await getC(con.url, con.params, con.withToken || true);
          if (err?.status_code === 401) dispatch(resetLogin());
          return [res, err];
        }
        if (con.method === 'post') {
          const [res, err] = await postC(con.url, con.params, con.withToken || true);
          if (err?.status_code === 401) dispatch(resetLogin());
          return [res, err];
        }
        // else if(con.method ==='put') {

        // }
        // else if(con.method ==='patch') {

        // }
        // else if(con.method ==='delete') {

        // }
      }),
    ]);

    setResults(() => resp);
    setLoading(() => false);
  };

  return [loading, results, cb];
};
