import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Button, Table, Tabs } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import axios from 'axios';

import { useGet } from '../hooks/useApi';
import { selectAllClassesSections, selectClasses } from '../stores/reducers/classSlice';
import Spin from './Spin';
import style from '../styles/components/StudentPromotion.module.scss';

const { TabPane } = Tabs;

export default () => {
  const [cls, setCls] = useState('');
  const [section, setSection] = useState('');
  // eslint-disable-next-line no-unused-vars
  const [response, error, loading, get] = useGet();
  const [stuData, setStuData] = useState([]);

  const allClassesData = useSelector(selectClasses);
  const [classes, sections] = useSelector(selectAllClassesSections);

  const columns = [
    {
      title: 'Avatar',
      dataIndex: 'avatar',
      // eslint-disable-next-line react/prop-types
      render: ({ src, alt }) => (
        <img src={src} alt={alt} style={{ height: '30px', width: '30px', borderRadius: '50%' }} />
      ),
    },
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Roll',
      dataIndex: 'roll',
    },
    {
      title: 'Action',
      dataIndex: 'action',
      render: () => {
        const [disabled, setDisabled] = useState(false);

        return (
          <Button disabled={disabled} danger onClick={() => setDisabled(!disabled)}>
            Not Promote
          </Button>
        );
      },
    },
    {
      title: 'Delete',
      dataIndex: 'delete',
      render: () => <Button danger type="primary" shape="round" icon={<DeleteOutlined />} />,
    },
  ];

  const rowSelection = {
    onChange: () => {
      // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name,
    }),
  };

  useEffect(() => {
    setCls(() => classes[0]);
    setSection(() => sections[0][0]);
  }, []);

  // eslint-disable-next-line consistent-return
  useEffect(() => {
    if (allClassesData && section !== '' && cls !== '') {
      const { id } = allClassesData[
        allClassesData.findIndex((o) => o.name === cls && o.section === section)
      ];

      const fetch = async () => {
        await get(`/api/v1/school/students/${id}/`);
      };

      fetch();
    }
  }, [section, cls]);

  useEffect(() => {
    if (response) {
      setStuData(() => [
        ...response.students.map((d) => ({
          key: d.id,
          avatar: {
            src: `${axios.defaults.baseURL.substring(0, axios.defaults.baseURL.length - 1)}${
              d.image
            }`,
            alt: `${d.name}-avatar`,
          },
          name: d.name,
          roll: d.rollno,
          action: 'NotPromoted',
          delete: 'Delete',
        })),
      ]);
    }
  }, [response]);

  return (
    <div className={style.studentPromotion}>
      <div>
        <div className={style.btn}>
          <div className={style.name1}>Class</div>
          <div>
            <Tabs
              defaultActiveKey={cls}
              onChange={(value) => {
                if (
                  sections[classes.findIndex((o) => o === value)].findIndex(
                    (o) => o === section,
                  ) === -1
                )
                  setSection(() => sections[classes.findIndex((o) => o === value)][0]);
                setCls(() => value);
              }}
            >
              {classes?.map((c) => (
                <TabPane tab={c} key={c} />
              ))}
            </Tabs>
          </div>
        </div>
        <div className={style.btn}>
          <div className={style.name2}>Section</div>
          <div>
            <Tabs
              defaultActiveKey={section}
              onChange={(value) => setSection(() => value)}
              activeKey={section}
            >
              {sections[classes?.findIndex((o) => o === cls) || 0]?.map((c) => (
                <TabPane tab={c} key={c} />
              ))}
            </Tabs>
          </div>
        </div>
      </div>
      <div>
        {loading ? (
          <Spin />
        ) : (
          <Table
            rowSelection={{
              type: 'checkbox',
              ...rowSelection,
            }}
            columns={columns}
            dataSource={stuData}
          />
        )}
      </div>
      {!loading && (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Button type="primary">Promote</Button>
        </div>
      )}
    </div>
  );
};
