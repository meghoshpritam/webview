import { Layout, Input, Divider, Button } from 'antd';
import { MoreOutlined } from '@ant-design/icons';
import style from '../styles/components/Slider.module.scss';

const { Sider } = Layout;
const { Search } = Input;

const ListItem = () => {
  return (
    <div className={style.listItem}>
      <div className={style.img}>XA</div>
      <div>xyz Department ABCD</div>
    </div>
  );
};

export default () => {
  return (
    <Sider theme="light" className={style.sideBar} width={320}>
      <div className={style.heading}>
        <div>Organization Name</div>
        <div>
          <Button
            type="text"
            icon={<MoreOutlined />}
            className={style.btn}
            // className={`${style.menuBtn} ${menuBtnCls}`}
            // onClick={() => setDrawer(true)}
          />
        </div>
      </div>

      <div className={style.head}>
        <div className={style.row2Text}>Teacher Assessment</div>
        <div>
          <Search placeholder="input search text" enterButton />
        </div>
      </div>

      <Divider />
      <div>
        <ListItem />
        <ListItem />
      </div>
    </Sider>
  );
};
