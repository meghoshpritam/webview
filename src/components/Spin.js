import { Spin } from 'antd';

import style from '../styles/components/Spin.module.scss';

export default () => {
  return (
    <div className={style.container}>
      <Spin size="large" />
    </div>
  );
};
