import { useState, useEffect } from 'react';
import { Tabs, Button, Drawer } from 'antd';
import { MenuOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import Dashboard from './Dashboard';
import StudentPromotion from './StudentPromotion';
import Slider from './Slider';
import Spin from './Spin';
import { useGet, useParallel } from '../hooks/useApi';
import { setClasses, setLoading, resetLoading } from '../stores/reducers/classSlice';
import style from '../styles/components/MainContent.module.scss';
import '../styles/notRecommended/drawerPadding.scss';

const { TabPane } = Tabs;

const MainContent = ({ menuBtnCls }) => {
  const [drawer, setDrawer] = useState(false);
  const [loading, result, parallel] = useParallel();
  const dispatch = useDispatch();

  useEffect(() => {
    const fetch = async () => {
      dispatch(setLoading());
      await parallel([{ method: 'get', url: '/api/v1/school/classes/' }]);
      dispatch(resetLoading());
    };

    fetch();

    return fetch;
  }, []);

  useEffect(() => {
    if (result) {
      dispatch(setClasses(result[0][0].classes));
    }
  }, [result]);

  return (
    <div className={style.mainContent}>
      <div className={style.heading}>
        <Button
          type="text"
          icon={<MenuOutlined />}
          className={`${style.menuBtn} ${menuBtnCls}`}
          onClick={() => setDrawer(true)}
        />
        <div>Activity Section</div>
        <div />
      </div>
      <Tabs defaultActiveKey="1">
        <TabPane tab="Dashboard" key="1">
          {loading ? <Spin /> : <Dashboard />}
        </TabPane>
        <TabPane tab="Student Promotion" key="2">
          <StudentPromotion />
        </TabPane>
        <TabPane tab="Exam" key="3" disabled>
          Exam Tab
        </TabPane>
        <TabPane tab="Add/Asign Teacher" key="4" disabled>
          Add/Asign Teacher
        </TabPane>
      </Tabs>
      <Drawer
        placement="left"
        closable={false}
        onClose={() => setDrawer(false)}
        visible={drawer}
        key="left"
        width={320}
        className={style.drawer}
      >
        <Slider />
      </Drawer>
    </div>
  );
};

MainContent.propTypes = {
  menuBtnCls: PropTypes.node.isRequired,
};

export default MainContent;
