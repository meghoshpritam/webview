import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Table } from 'antd';

import { useGet } from '../hooks/useApi';
import Spin from './Spin';
import { selectLoading, selectClassTableData } from '../stores/reducers/classSlice';
import style from '../styles/components/Dashboard.module.scss';

export default () => {
  const [response, error, loading, get] = useGet();
  const [userStat, setUserStat] = useState({ totalUsers: 0, activeUsers: 0 });
  const classLoading = useSelector(selectLoading);
  const data = useSelector(selectClassTableData);

  const columns = [
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',
      sorter: (a, b) => (a.class < b.class ? -1 : 1),
    },
    {
      title: 'Enrolled Student',
      dataIndex: 'student',
      key: 'student',
      sorter: (a, b) => (a.student < b.student ? -1 : 1),
    },
    {
      title: 'Info',
      dataIndex: 'info',
      key: 'info',
      render: (text) => <a>{text}</a>,
    },
  ];

  useEffect(() => {
    const fetch = async () => {
      await get('/api/v1/school/active/');
    };

    fetch();

    return fetch;
  }, []);

  useEffect(() => {
    if (response)
      setUserStat({ totalUsers: response.total_users, activeUsers: response.active_users });
  }, [response]);

  return (
    <div className={style.container}>
      {loading ? (
        <Spin />
      ) : (
        <div className={style.activity}>
          <div className={style.top}>
            <div>Active users</div>
            <div>{userStat.activeUsers}</div>
          </div>
          <div className={style.bottom}>
            <div>Total Students</div>
            <div>{userStat.totalUsers}</div>
          </div>
        </div>
      )}
      <Button type="primary">Publish Notice</Button>
      <div className={style.table}>
        {classLoading ? (
          <Spin />
        ) : (
          <Table
            title={() => <div className={style.tableHeading}>Class wise Student Data</div>}
            columns={columns}
            dataSource={data}
          />
        )}
      </div>
    </div>
  );
};
