import { useState, useEffect } from 'react';
import { Input, Button, Form } from 'antd';
import {
  UserOutlined,
  MobileOutlined,
  UnlockOutlined,
  SendOutlined,
  EditOutlined,
} from '@ant-design/icons';
import { useDispatch } from 'react-redux';

import { usePost } from '../hooks/useApi';
import { setLogin } from '../stores/reducers/loginSlice';
import style from '../styles/pages/Login.module.scss';

export default () => {
  const [state, setState] = useState({ phoneNumber: '', otp: '', otpSend: false });
  const [form] = Form.useForm();
  const [response, error, loading, post] = usePost();
  const dispatch = useDispatch();

  const handelChange = (name) => (e) => {
    let val = e.target.value.substring(0, 10);
    if (name === 'phoneNumber') {
      val = '';

      for (let idx = 0; idx < e.target.value.length; idx += 1)
        if (e.target.value[idx] >= '0' && e.target.value[idx] <= '9') val += e.target.value[idx];
      let tem = val;
      if (val.length > 2) tem = `${val.substring(0, 3)} `;
      if (val.length > 3) tem += `${val.substring(3, 6)} `;
      if (val.length > 6) tem += val.substring(6, 10);
      val = tem.trim();
    }

    setState((s) => ({ ...s, [name]: val }));
  };

  const handelSubmit = async () => {
    let phone = '';
    for (let idx = 0; idx < state.phoneNumber.length; idx += 1) {
      if (state.phoneNumber[idx] >= '0' && state.phoneNumber[idx] <= '9')
        phone += state.phoneNumber[idx];
    }
    if (state.otpSend) await post('/auth/signin/4/2/', { phoneno: phone, otp: state.otp }, false);
    else await post('/auth/signin/4/1/', { phoneno: phone }, false);
  };

  useEffect(() => {
    if (state.otpSend && response) dispatch(setLogin(response.token));
    else if (response) setState((s) => ({ ...s, otpSend: true }));
  }, [response]);

  return (
    <div className={style.container}>
      <div className={style.box}>
        <div>
          <h1>Welcome to Slient</h1>
        </div>
        <div>
          <UserOutlined className={style.icon} />
        </div>
        <div>
          <h2>Sign In</h2>
        </div>
        <Form form={form} layout="vertical">
          <Form.Item
            label="Mobile Number"
            required
            tooltip="Enter your registered mobile number"
            validateStatus={!state.otpSend && error ? 'error' : 'success'}
            help={!state.otpSend && error?.message ? error?.message : ''}
          >
            <Input
              placeholder="9876543210"
              prefix={<MobileOutlined />}
              size="large"
              value={state.phoneNumber}
              disabled={state.otpSend}
              onChange={handelChange('phoneNumber')}
              autoFocus
            />
          </Form.Item>
          {state.otpSend && (
            <>
              <Form.Item>
                <Button
                  type="default"
                  icon={<EditOutlined />}
                  className={style.btn}
                  size="small"
                  shape="round"
                  onClick={() => setState((s) => ({ ...s, otp: '', otpSend: false }))}
                >
                  Change Mobile Number
                </Button>
              </Form.Item>
              <Form.Item label="OTP" required tooltip="Enter the otp send in your mobile">
                <Input
                  placeholder="123456"
                  prefix={<MobileOutlined />}
                  size="large"
                  value={state.otp}
                  onChange={handelChange('otp')}
                  disabled={!state.otpSend}
                  autoFocus
                />
              </Form.Item>
            </>
          )}
          <Form.Item>
            <Button
              type="primary"
              icon={state.otpSend ? <UnlockOutlined /> : <SendOutlined />}
              loading={loading}
              className={style.btn}
              size="large"
              shape="round"
              disabled={
                state.otpSend && response && !loading
                  ? state.otp.length < 6
                  : state.phoneNumber.length < 12
              }
              onClick={handelSubmit}
            >
              {state.otpSend ? 'Sign In' : 'Send OTP'}
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
