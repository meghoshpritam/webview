import { configureStore } from '@reduxjs/toolkit';

import loginReducer from './reducers/loginSlice';
import counterReducer from './reducers/counterSlice';
import classReducer from './reducers/classSlice';

export default configureStore({
  reducer: {
    login: loginReducer,
    counter: counterReducer,
    class: classReducer,
  },
});
