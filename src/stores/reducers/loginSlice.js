import { createSlice } from '@reduxjs/toolkit';

export const loginSlice = createSlice({
  name: 'login',
  initialState: false,
  reducers: {
    setLogin: (state, action) => {
      localStorage.setItem('token', action.payload);
      return true;
    },
    resetLogin: () => {
      localStorage.removeItem('token');
      return false;
    },
    checkLogin: () => {
      if (
        localStorage.getItem('token') &&
        localStorage.getItem('token') !== 'undefined' &&
        localStorage.getItem('token') !== ''
      )
        return true;

      localStorage.removeItem('token');
      return false;
    },
  },
});

export const { setLogin, resetLogin, checkLogin } = loginSlice.actions;

export const selectLogin = (state) => state.login;

export default loginSlice.reducer;
