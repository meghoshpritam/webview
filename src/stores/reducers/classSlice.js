import { createSlice } from '@reduxjs/toolkit';

export const classSlice = createSlice({
  name: 'class',
  initialState: {
    classes: [],
    loading: false,
  },
  reducers: {
    setClasses: (state, action) => {
      return { ...state, classes: [...action.payload] };
    },
    setLoading: (state) => ({ ...state, loading: true }),
    resetLoading: (state) => ({ ...state, loading: false }),
  },
});

export const { setClasses, resetLoading, setLoading } = classSlice.actions;

export const selectClasses = (state) => state.class.classes;
export const selectLoading = (state) => state.class.loading;

export const selectClassTableData = (state) => {
  let data = [];

  state.class.classes.forEach((d) => {
    data = [
      ...data,
      {
        class: `${d.name}, Section ${d.section}`,
        key: d.id,
        student:
          d.enrolled_students > 0
            ? `${d.enrolled_students} students enrolled`
            : 'No student enrolled',
        info: `More information`,
      },
    ];
  });

  return data;
};

export const selectAllClassesSections = (state) => {
  let classes = [];
  const sections = [];

  state.class.classes.forEach((d) => {
    if (classes.findIndex((o) => o === d.name) === -1) classes = [...classes, d.name];
  });

  classes.sort((a, b) => (a > b ? 1 : -1));

  state.class.classes.forEach((d) => {
    if (sections[classes.findIndex((o) => o === d.name)])
      sections[classes.findIndex((o) => o === d.name)] = [
        ...sections[classes.findIndex((o) => o === d.name)],
        d.section,
      ];
    else sections[classes.findIndex((o) => o === d.name)] = [d.section];
  });

  for (let idx = 0; idx < sections.length; idx += 1) sections[idx].sort((a, b) => (a > b ? 1 : -1));

  return [classes, sections];
};

export default classSlice.reducer;
